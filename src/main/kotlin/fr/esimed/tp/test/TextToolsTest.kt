package fr.esimed.tp.test

import levenshteinDistance
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable
import percentMatch


class TextToolsTest {

    @Test
    fun levenshteinDistanceTest(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(6, levenshteinDistance("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(1, levenshteinDistance("Julie", "Julien")) }
        )

        Assertions.assertAll(
            Executable { Assertions.assertEquals(50, percentMatch("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(90, percentMatch("Julie", "Julien")) }
        )
    }

}