import kotlin.math.floor
import kotlin.math.roundToInt

fun levenshteinDistance(Chaine1:String, Chaine2:String): Int {

    val Ch1Length = Chaine1.length
    val Ch2Length = Chaine2.length

    var cost = IntArray(Ch1Length + 1) { it }
    var newCost = IntArray(Ch2Length + 1) { 0 }

    for (i in 1..Ch2Length) {
        newCost[0] = i

        for (j in 1..Ch1Length) {
            val editCost= if(Chaine1[j - 1] == Chaine2[i - 1]) 0 else 1

            val costReplace = cost[j - 1] + editCost
            val costInsert = cost[j] + 1
            val costDelete = newCost[j - 1] + 1

            newCost[j] = minOf(costInsert, costDelete, costReplace)
        }

        val swap = cost
        cost = newCost
        newCost = swap
    }

    return cost[Ch1Length]

}

fun percentMatch(Chaine1:String,Chaine2:String): Int{
    var total:Float = ( 100f - ( levenshteinDistance(Chaine1, Chaine2) * 100f ) / ( Chaine1.length + Chaine2.length) )
    return total.toInt()
}